package ru.sbrf.edu;

import java.util.Collection;

public class CustomArrayImpl<T> implements CustomArray<T>{
    private Object[] obj;
    private int size;

    public CustomArrayImpl() {
        this.size = 0;
        this.obj = new Object[this.size];
    }

    public CustomArrayImpl(int capacity) {
        this.size = capacity;
        this.obj = new Object[this.size];
    }

    public CustomArrayImpl(Collection<T> collection) {
        this.size = collection.size();
        this.obj = new Object[this.size];
        System.arraycopy(collection.toArray(), 0, this.obj, 0, collection.size());
    }

    public int size() {
        return this.size;
    }

    public boolean isEmpty() {
        return this.size == 0;
    }

    public boolean add(T item) {
        try {
            Object[] newObj = new Object[this.size++];
            System.arraycopy(this.obj, 0, newObj, 0, newObj.length - 1);
            newObj[this.size] = item;
            this.obj = newObj;
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    public boolean addAll(T[] items) throws IllegalArgumentException {
        if (items == null) throw new IllegalArgumentException();
        try {
            Object[] newObj = new Object[this.size + items.length];
            System.arraycopy(this.obj, 0, newObj, 0, this.obj.length);
            System.arraycopy(items, 0, newObj, this.size, items.length);
            this.size = this.size + items.length;
            this.obj = newObj;
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    public boolean addAll(Collection<T> items) throws IllegalArgumentException {
        if (items == null) throw new IllegalArgumentException();
        try {
            Object[] newObj = new Object[this.size + items.size()];
            System.arraycopy(this.obj, 0, newObj, 0, this.obj.length);
            System.arraycopy(items.toArray(), 0, newObj, this.size, items.size());
            this.size = this.size + items.size();
            this.obj = newObj;
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    public boolean addAll(int index, T[] items) throws ArrayIndexOutOfBoundsException, IllegalArgumentException {
        if (index >= this.size) throw new ArrayIndexOutOfBoundsException();
        if (items == null) throw new IllegalArgumentException();
        try {
            Object[] newObj = new Object[this.size + items.length];
            System.arraycopy(this.obj, 0, newObj, 0, index);
            System.arraycopy(items, 0, newObj, index, items.length);
            System.arraycopy(this.obj, index, newObj, index + items.length, items.length-index);
            this.obj = newObj;
            this.size = newObj.length;
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    public T get(int index) {
        if (index >= this.size) throw new ArrayIndexOutOfBoundsException();
        return (T) this.obj[index];
    }

    public T set(int index, T item) {
        if (index >= this.size) throw new ArrayIndexOutOfBoundsException();
        T old = (T) this.obj[index];
        this.obj[index] = item;
        return old;
    }

    public void remove(int index) {
        if (index >= this.size) throw new ArrayIndexOutOfBoundsException();
        Object[] newObj = new Object[this.size - 1];
        System.arraycopy(this.obj, 0, newObj, 0, index);
        System.arraycopy(this.obj, index + 1, newObj, index, newObj.length - index);
        this.obj = newObj;
        this.size = newObj.length;
    }

    public boolean remove(T item) {
        if (this.indexOf(item) == -1) {
            return false;
        }
        try {
            this.remove(this.indexOf(item));
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    public boolean contains(T item) {
        return this.indexOf(item) >= 0;
    }

    public int indexOf(T item) {
        for (int i = 0; i < this.obj.length; i++) {
            if (this.obj[i].equals(item))
                return i;
        }
        return -1;
    }

    public void ensureCapacity(int newElementsCount) {
        Object[] newObj = new Object[newElementsCount];
        System.arraycopy(this.obj, 0, newObj, 0, this.size);
        this.size = newElementsCount;
        this.obj = newObj;
    }

    public int getCapacity() {
        return this.size;
    }

    public void reverse() {
        Object[] newObj = new Object[this.size];
        for (int i = 0; i < this.obj.length; i++) {
            newObj[i] = this.obj[this.size - i - 1];
        }
        this.obj = newObj;

    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[ ");
        for (Object item : this.obj) {
            stringBuilder.append(item.toString()).append(' ');
        }
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    public Object[] toArray() {
        Object[] newObj = new Object[this.size];
        System.arraycopy(this.obj, 0, newObj, 0, this.obj.length);
        return newObj;
    }
}