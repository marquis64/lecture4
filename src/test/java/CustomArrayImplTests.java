import org.junit.Assert;
import org.junit.Test;
import ru.sbrf.edu.CustomArrayImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;


public class CustomArrayImplTests {

    CustomArrayImpl<String> array = new CustomArrayImpl<String>();

    @Test
    public void size_Success() {
        Assert.assertEquals(0, array.size());
    }

    @Test
    public void isEmpty_Success() {
        Assert.assertTrue( array.isEmpty());
    }

    @Test
    public void add_Success() {
        array.add("q");
        array.add("w");
        Assert.assertEquals(2, array.size());
    }

    @Test
    public void addAll_Success() {
        array.add("q");
        array.add("w");

        String[] str = {"e", "r", "t"};
        array.addAll(str);

        Assert.assertEquals(5, array.size());
    }

    @Test
    public void addAll_IllegalArgumentException() {
        try {
            String[] str = null;
            array.addAll(str);
        } catch (IllegalArgumentException thrown) {
            Assert.assertNull(thrown.getMessage());
        }
    }

    @Test
    public void addAllCollection_Success() {
        array.add("q");
        array.add("w");
        Collection<String> collection = new ArrayList<String>(Arrays.asList("e", "r", "t"));
        array.addAll(collection);
        Assert.assertEquals(5, array.size());
    }

    @Test
    public void addAllCollection_IllegalArgumentException() {
        try {
            Collection<String> collection = null;
            array.addAll(collection);
        } catch (IllegalArgumentException thrown) {
            Assert.assertNull(thrown.getMessage());
        }
    }

    @Test
    public void addAllIndex_Success() {
        CustomArrayImpl<String> array = new CustomArrayImpl<String>(Arrays.asList("1", "2", "3", "9", "10"));
        String[] items = {"4", "5", "6", "7", "8"};
        array.addAll(3,items);
        Assert.assertEquals("[ 1 2 3 4 5 6 7 8 9 10 ]", array.toString());
    }

    @Test
    public void addAllIndex_ArrayIndexOutOfBoundsException() {
        try {
            CustomArrayImpl<String> array = new CustomArrayImpl<String>(Arrays.asList("1", "2", "3", "9", "10"));
            String[] items = {"4", "5", "6", "7", "8"};
            array.addAll(6,items);
        } catch (ArrayIndexOutOfBoundsException thrown) {
            Assert.assertNull(thrown.getMessage());
        }
    }

    @Test
    public void addAllIndex_IllegalArgumentException() {
        try {
            CustomArrayImpl<String> array = new CustomArrayImpl<String>(Arrays.asList("1", "2", "3", "9", "10"));
            array.addAll(1,null);
        } catch (IllegalArgumentException thrown) {
            Assert.assertNull(thrown.getMessage());
        }
    }

    @Test
    public void get_Success() {
        CustomArrayImpl<String> array = new CustomArrayImpl<String>(Arrays.asList("1", "2", "3", "4", "5"));
        Assert.assertEquals("4", array.get(3));
    }

    @Test
    public void get_ArrayIndexOutOfBoundsException() {
        try {
            CustomArrayImpl<String> array = new CustomArrayImpl<String>(Arrays.asList("1", "2", "3", "4", "5"));
            array.get(5);
        } catch (ArrayIndexOutOfBoundsException thrown) {
            Assert.assertNull(thrown.getMessage());
        }
    }

    @Test
    public void set_Success() {
        CustomArrayImpl<String> array = new CustomArrayImpl<String>(Arrays.asList("1", "2", "3", "4", "5"));
        Assert.assertEquals("2", array.set(1, "11"));
        Assert.assertEquals("11", array.get(1));
    }

    @Test
    public void set_ArrayIndexOutOfBoundsException() {
        try {
            CustomArrayImpl<String> array = new CustomArrayImpl<String>(Arrays.asList("1", "2", "3", "4", "5"));
            array.set(5, "11");
        } catch (ArrayIndexOutOfBoundsException thrown) {
            Assert.assertNull(thrown.getMessage());
        }
    }

    @Test
    public void removeIndex_Success() {
        CustomArrayImpl<String> array = new CustomArrayImpl<String>(Arrays.asList("1", "2", "3", "4", "5"));
        array.remove(3);
        Assert.assertEquals("5", array.get(3));
    }

    @Test
    public void removeIndex_ArrayIndexOutOfBoundsException() {
        try {
            CustomArrayImpl<String> array = new CustomArrayImpl<String>(Arrays.asList("1", "2", "3", "4", "5"));
            array.remove(10);
        } catch (ArrayIndexOutOfBoundsException thrown) {
            Assert.assertNull(thrown.getMessage());
        }
    }

    @Test
    public void removeItem_Success() {
        CustomArrayImpl<String> array = new CustomArrayImpl<String>(Arrays.asList("1", "2", "3", "4", "5"));
        array.remove("3");
        Assert.assertEquals("4", array.get(2));
    }

    @Test
    public void contains_Success() {
        CustomArrayImpl<String> array = new CustomArrayImpl<String>(Arrays.asList("1", "2", "3", "4", "5"));
        Assert.assertTrue(array.contains("3"));
    }

    @Test
    public void contains_Success2() {
        CustomArrayImpl<String> array = new CustomArrayImpl<String>(Arrays.asList("1", "2", "3", "4", "5"));
        Assert.assertFalse(array.contains("6"));
    }

    @Test
    public void indexOf_Success() {
        CustomArrayImpl<String> array = new CustomArrayImpl<String>(Arrays.asList("1", "2", "3", "4", "5"));
        Assert.assertEquals(2, array.indexOf("3"));
    }

    @Test
    public void indexOf_Success2() {
        CustomArrayImpl<String> array = new CustomArrayImpl<String>(Arrays.asList("1", "2", "3", "4", "5"));
        Assert.assertEquals(-1, array.indexOf("6"));
    }

    @Test
    public void ensureCapacity_Success() {
        array.ensureCapacity(5);
        Assert.assertEquals(5, array.size());
    }

    @Test
    public void getCapacity_Success() {
        array.ensureCapacity(5);
        Assert.assertEquals(5, array.getCapacity());
    }

    @Test
    public void reverse_Success() {
        CustomArrayImpl<String> array = new CustomArrayImpl<String>(Arrays.asList("1", "2", "3", "4", "5"));
        array.reverse();
        Assert.assertEquals("[ 5 4 3 2 1 ]", array.toString());
    }

    @Test
    public void toString_Success() {
        CustomArrayImpl<String> array = new CustomArrayImpl<String>(Arrays.asList("1", "2", "3", "4", "5"));
        Assert.assertEquals("[ 1 2 3 4 5 ]", array.toString());
    }

    @Test
    public void toArray_Success() {
        String[] data = new String[] {"1", "2", "3", "4", "5"};
        CustomArrayImpl<String> array = new CustomArrayImpl(Arrays.asList(data));
        Assert.assertArrayEquals(data, array.toArray());
    }
}
